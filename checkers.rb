class InvalidMoveError < StandardError
end

class Piece
  attr_accessor :pos, :king, :display
  attr_reader :color, :board

  def initialize(pos, color, board)
    @pos = pos
    @color = color
    @board = board
    @king  = false
    @display = ''
  end

  def display
    self.color[0]
  end

  def deltas
    [[1,1], [1,-1], [-1, 1], [-1,-1]]
  end
  
  def king_me?(row)
    row == 0 || row == 7
  end
  
  def king_me
    self.king = true
    self.display = self.color[0].upcase
  end
  
  def move(delta_move, pos= self.pos)
    end_pos = pos[0] + delta_move[0], pos[1] + delta_move[1]
  end

  def on_board?(move_pos)
    (0..7).include?(move_pos[0]) && (0..7).include?(move_pos[1])
  end

  def slide_moves
    slide_moves = []
    deltas.each do |delt|
      move_pos = move(delt)
      slide_moves << move_pos
    end

    slide_moves
  end

  def jump_moves
    jump_moves = []
    deltas.each do |delt|
      move_pos = move(delt, move(delt))
      jump_moves << move_pos
    end
    jump_moves
  end

  def black_moves(arr)
    arr = [arr[2], arr[3]] unless self.king
    arr.select { |el| on_board?(el) }
  end

  def white_moves(arr)
    arr = [arr[0], arr[1]] unless self.king
    arr.select { |el| on_board?(el) }
  end

  def perform_slide?(end_pos)
    return black_moves(slide_moves).include?(end_pos) if self.color == 'black'
    return white_moves(slide_moves).include?(end_pos) if self.color == 'white'
  end

  def perform_jump?(end_pos)
    return black_moves(jump_moves).include?(end_pos) if self.color == 'black'
    return white_moves(jump_moves).include?(end_pos) if self.color == 'white'
  end

  def move_to_nil?(pos)
    board.get_space(pos) == nil
  end

  def move_piece(end_pos)
    self.board.grid[pos[0]][pos[1]] = nil
    self.pos = end_pos
    self.board.grid[pos[0]][pos[1]] = Piece.new(end_pos, self.color, self.board)
    king_me if king_me?(end_pos[0])
  end

  def perform_slide(end_pos)
    if perform_slide?(end_pos)
      if move_to_nil?(end_pos)
        move_piece(end_pos)
      end

    else
      raise InvalidMoveError.new("Invalid Slide")
    end
  end

  def inbetween_jump(start_pos, end_pos)
    inbetween = (start_pos[0] + end_pos[0]) / 2, (start_pos[1] + end_pos[1]) / 2
  end

  def opposite_color?(pos)
    end_pos = inbetween_jump(self.pos, pos)
    self.color != self.board.grid[end_pos[0]][end_pos[1]].color
  end

  def perform_jump(end_pos)
    middle = inbetween_jump(self.pos, end_pos)
    if perform_jump?(end_pos)
      if move_to_nil?(end_pos) && opposite_color?(end_pos)
        self.board.grid[middle[0]][middle[1]] = nil
        move_piece(end_pos)
      end

    else
      raise InvalidMoveError.new("Invalid Jump")
    end
  end

  def num_to_array(num)
    arr = num/10, num%10
  end
  
  def valid_move_seq?(sequence_arr)
    new_board = self.board.dup_board
    begin
      new_board.grid[pos[0]][pos[1]].perform_moves!(sequence_arr)
    rescue InvalidMoveError => e
      puts "#{e.message}"
      false
    else
      true
    end
  end
  
  def perform_moves!(move_sequence)
    moves_arr = []

    move_sequence.each_with_index do |moves, i|
      if slide_moves.include?(moves) && move_sequence > 1
        raise InvalidMoveError.new("Too many sliding moves!")
      else
        if moves[0] == "Slide"
          self.perform_slide(num_to_array(moves[1].to_i))
        elsif moves[0] == "Jump"
          self.perform_jump(num_to_array(moves[1].to_i))
        else
          raise InvalidMoveError.new("Invalid Input")
        end

      end
    end
  end
  
  def perform_moves(move_sequence)
    if valid_move_seq?(move_sequence)
      perform_moves!(move_sequence)
    else
      raise InvalidMoveError("Bad Sequence...")
    end
  end

end

class Board
  attr_accessor :grid

  def initialize(init = true)
    @grid = Array.new(8){ Array.new(8) }
    initialize_white if init
    initialize_black if init
  end

  def on_black_space?(row, col)
    (row + col) % 2 == 1
  end

  def initialize_white
    (0..2).each do |row|
      (0..7).each do |col|
        self.grid[row][col] = Piece.new([row, col], "white", self) if on_black_space?(row, col)
      end
    end
  end

  def initialize_black
    (5..7).each do |row|
      (0..7).each do |col|
        self.grid[row][col] = Piece.new([row, col], "black", self) if on_black_space?(row, col)
      end
    end
  end

  def get_space(pos)
    self.grid[pos[0]][pos[1]]
  end

  def dup_board
    dup_board = Board.new(false)
    self.grid.each_with_index do |row, row_i|
      row.each_with_index do |space, col_i|
        next if space.nil?
        dup_board.grid[row_i][col_i] = Piece.new(space.pos, space.color, dup_board)
      end
    end
    dup_board
  end

  def display
    self.grid.map do |row|
      row.map do |space|
        print " #{space.display} " if space.is_a?(Piece)
        print " * " if space.nil?
      end

      puts
      puts
    end
  end
end

class Game
  attr_accessor :board

  def initialize #didnt get turns to switch
    @board = Board.new
    play
  end

  def num_to_array(num)
    arr = num/10, num%10
  end

  def play
    loop do
      board.display
      puts "Which piece would you like to move (rowcol... Ex:07 => top right corner)"
      start_coord = gets.chomp.to_i
      puts "Slide or Jump? To where? (Slide,54 OR Jump,22-Jump,44)"
      move = gets.chomp
      move_arr = move.split('-')
      piece = board.get_space(num_to_array(start_coord))

      sequence_arr = []
      move_arr.each do |move_name|
        sequence_arr << move_name.split(',')
      end

      piece.perform_moves(sequence_arr)
    end
  end


end

Game.new
